/// <reference types="cypress" />

describe("QAAutomationLabs.com", { testIsolation: false }, () => {
  it("Open URL", () => {
    cy.visit("https://qaautomationlabs.com/");
  });
  it("Click on Blogs", () => {
    cy.contains("Blog").scrollIntoView().click({ force: true });
  });
});
